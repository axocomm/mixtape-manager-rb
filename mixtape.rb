require 'json'

require_relative './errors.rb'

module MixtapeManager

  # The class Mixtape.
  #
  # This class encapsulates the various details of a given mixtape,
  # containing attributes for users, playlists, and songs.
  class Mixtape
    attr_reader :users, :playlists, :songs

    # Initialise a new Mixtape.
    #
    # This populates the `users`, `playlists`, and `songs` attributes
    # given the hash map provided in `params`.
    def initialize(params = {})
      @users = params[:users] || []
      @playlists = params[:playlists] || []
      @songs = params[:songs] || []
    end

    # Add a playlist to this Mixtape.
    #
    # This creates a new playlist given a user and song IDs. This will
    # throw if the user given does not exist, no songs are provided, or
    # any songs provided do not exist in `@songs`.
    def add_playlist!(user_id, song_ids)
      unless user_exists?(user_id)
        raise Errors::NoSuchUser, user_id
      end

      if !song_ids || song_ids.empty?
        raise Errors::MissingSongs
      end

      missing_songs = song_ids.reject { |id| song_exists?(id) }
      unless missing_songs.empty?
        raise Errors::NoSuchSongs, missing_songs
      end

      new_playlist = make_playlist(user_id, song_ids)
      @playlists = @playlists.push(new_playlist)
    end

    # Add a new song to an existing playlist.
    #
    # This adds a song specified by `song_id` to a given playlist
    # `playlist_id`. If no such playlist or song exists, an exception is
    # thrown.
    def add_song!(playlist_id, song_id)
      unless playlist_exists?(playlist_id)
        raise Errors::NoSuchPlaylist, playlist_id
      end

      unless song_exists?(song_id)
        raise Errors::NoSuchSong, song_id
      end

      @playlists = @playlists.map do |pl|
        if pl[:id] == playlist_id
          pl.merge(song_ids: pl[:song_ids].push(song_id).uniq)
        else
          pl
        end
      end
    end

    # Remove an existing playlist.
    #
    # This removes the playlist specified by `playlist_id`. If the
    # playlist does not exist, an exception is thrown.
    def remove_playlist!(playlist_id)
      unless playlist_exists?(playlist_id)
        raise Errors::NoSuchPlaylist, playlist_id
      end

      @playlists = @playlists.reject { |pl| pl[:id] == playlist_id }
    end

    # Find a user by ID.
    def find_user(user_id)
      @users.detect { |u| u[:id] == user_id }
    end

    # Check whether the given user exists.
    def user_exists?(user_id)
      not find_user(user_id).nil?
    end

    # Find a playlist by ID.
    def find_playlist(playlist_id)
      @playlists.detect { |pl| pl[:id] == playlist_id }
    end

    # Check whether the given playlist exists.
    def playlist_exists?(playlist_id)
      not find_playlist(playlist_id).nil?
    end

    # Find a song by ID.
    def find_song(song_id)
      @songs.detect { |s| s[:id] == song_id }
    end

    # Check whether the given song exists.
    def song_exists?(song_id)
      not find_song(song_id).nil?
    end

    # Generate a new playlist for use in `@playlists`, including
    # automatically incrementing the last ID in the collection for use
    # in the new entry.
    def make_playlist(user_id, song_ids)
      {
        id: Mixtape.next_id(@playlists),
        user_id: user_id,
        song_ids: song_ids
      }
    end

    # Generate a Hash of this Mixtape.
    def to_h
      {
        users: @users,
        playlists: @playlists,
        songs: @songs
      }
    end

    # JSONify this Mixtape.
    def to_json
      to_h.to_json
    end

    # Try to determine the next ID of the given collection.
    #
    # If no element exists in `coll`, `default` is returned instead.
    def self.next_id(coll, default = 1)
      ids = coll.map { |x| x[:id].to_i }
      return default if ids.empty?

      (ids.max + 1).to_s
    end

    # Parse a JSON file into a Mixtape instance.
    def self.from_json(filename)
      File.open(filename) do |fh|
        mixtape = JSON.parse(fh.read, symbolize_names: true)
        new(mixtape)
      end
    end
  end
end
