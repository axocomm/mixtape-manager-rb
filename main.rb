require 'thor'

require_relative './changes.rb'
require_relative './mixtape.rb'

def load_mixtape(filename)
  MixtapeManager::Mixtape.from_json(filename)
end

def load_changes(filename)
  MixtapeManager::Changeset.from_json(filename)
end

module MixtapeManager
  class Cli < Thor
    desc 'apply MIXTAPE CHANGES', 'Apply CHANGES to the given MIXTAPE'
    def apply(mixtape_file, changes_file)
      mixtape = load_mixtape(mixtape_file)
      changes = load_changes(changes_file)
      changes.apply!(mixtape)

      puts mixtape.to_json
    end
  end
end

if $PROGRAM_NAME == __FILE__
  MixtapeManager::Cli.start(ARGV)
end
