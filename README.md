# mixtape-manager

A simple mixtape manager that accepts a JSON payload representing a
mixtape and allows for application of several types of changes
thereto.

## Requirements

- Ruby

## Usage

### Setup

This project requires the `thor` and `rspec` gems, both of which are
specified in the `Gemfile`. To install these, simply run `bundle`.

### Running

The main entry point of this application is in `main.rb` and contains
a single command `apply` which accepts JSON files for mixtape and
changes to apply. For example, to run this using the example files
provided, simply invoke as follows:

    % ruby main.rb apply mixtape.json changes.json

This should produce new JSON output containing the mixtape with the
given changes applied.

## Testing

A collection of RSpec tests are included inside the `spec` directory:

    spec
    ├── actions_spec.rb
    ├── helpers.rb
    ├── mixtape_spec.rb
    └── spec_helper.rb

which evaluate basic functionality of this application.

To run these tests, simply invoke RSpec directly, which will run all
tests defined in the `*_spec` files:

    % rspec
    .....................

    Finished in 0.00945 seconds (files took 0.09121 seconds to load)
    21 examples, 0 failures

## How it Works

This application simply loads a mixtape and changeset from JSON into
respective objects (`Mixtape` and `Changeset`). Once these are set up,
the `Changeset#apply!` method is called which accepts a
`Mixtape`. This method then, depending on the individual change type,
instantiates an `Action` subclass, each of which contains its own
implementation-specific `#apply!` method which performs the intended
action on the mixtape.

Should any exceptional cases arise (including an unsupported action
type), an exception is raised and the program is halted.
