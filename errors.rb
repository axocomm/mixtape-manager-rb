module MixtapeManager

  # Mixtape-related errors.
  module Errors

    # Top-level Mixtape::Error.
    class Error < StandardError
    end

    # No such user exists.
    class NoSuchUser < Error
    end

    # No such playlist exists.
    class NoSuchPlaylist < Error
    end

    # No such song exists.
    class NoSuchSong < Error
    end

    # No such songs exist.
    class NoSuchSongs < Error
    end

    # No songs were provided.
    class MissingSongs < Error
    end

    # Invalid action type.
    class InvalidAction < Error
    end
  end
end
