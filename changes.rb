require 'json'

module MixtapeManager

  # The class Changeset.
  #
  # This class represents a set of modifications to be applied to a
  # Mixtape.
  class Changeset
    def initialize(changes = [])
      @changes = changes
    end

    # Apply the assigned changes as specified by `@changes` to the
    # given `mixtape`.
    def apply!(mixtape)
      @changes.each do |change|
        action = Action.dispatch(change)
        action.apply!(mixtape)
      end

      mixtape
    end

    # Initialise a new Changeset from a JSON file.
    def self.from_json(filename)
      File.open(filename) do |fh|
        changes = JSON.parse(fh.read, symbolize_names: true)
        new(changes)
      end
    end
  end

  # The abstract class Action.
  #
  # This class is the superclass to all mixtape actions. It features a
  # ::dispatch method which instantiates the appropriate subclass
  # using the payload provided by `action_rec`. If the given
  # `action_rec` specifies an unknown action type, an exception is
  # thrown instead.
  class Action

    # Abstract method for applying a change to the given Mixtape.
    def apply!(_mixtape, _params = {})
      raise NotImplemented, 'Must implement #apply!'
    end

    # Dispatch the given change by initialising the appropriate
    # `Action` subclass with an action payload.
    def self.dispatch(action_rec)
      action = action_rec[:action]
      payload = action_rec[:payload]

      case action
      when AddPlaylist::ACTION
        AddPlaylist.new(payload)
      when AddSong::ACTION
        AddSong.new(payload)
      when RemovePlaylist::ACTION
        RemovePlaylist.new(payload)
      else
        raise Errors::InvalidAction, action
      end
    end
  end

  # Add a new playlist.
  class AddPlaylist < Action
    ACTION = 'ADD_PLAYLIST'

    def initialize(params = {})
      @user_id = params[:user_id]
      @song_ids = params[:song_ids]
    end

    def apply!(mixtape)
      mixtape.add_playlist!(@user_id, @song_ids)
    end
  end

  # Add a song to an existing playlist.
  class AddSong < Action
    ACTION = 'ADD_SONG'

    def initialize(params = {})
      @playlist_id = params[:playlist_id]
      @song_id = params[:song_id]
    end

    def apply!(mixtape)
      mixtape.add_song!(@playlist_id, @song_id)
    end
  end

  # Remove an existing playlist.
  class RemovePlaylist < Action
    ACTION = 'REMOVE_PLAYLIST'

    def initialize(params = {})
      @playlist_id = params[:playlist_id]
    end

    def apply!(mixtape, params = {})
      mixtape.remove_playlist!(@playlist_id)
    end
  end
end
