require_relative '../mixtape.rb'

TEST_MIXTAPE = {
  users: [
    {
      id: '1',
      name: 'Foo'
    }
  ],

  playlists: [
    {
      id: '1',
      user_id: '1',
      song_ids: [
        '1',
        '2',
        '3'
      ]
    }
  ],

  songs: [
    {
      id: '1',
      artist: 'Foo',
      title: 'Bar'
    },
    {
      id: '2',
      artist: 'Baz',
      title: 'Quux'
    },
    {
      id: '3',
      artist: 'Spam',
      title: 'Eggs'
    },
    {
      id: '4',
      artist: 'Ran out of',
      title: 'Examples'
    }
  ]
}.freeze

module Helpers
  def new_mock_mixtape
    MixtapeManager::Mixtape.new(TEST_MIXTAPE)
  end
end
