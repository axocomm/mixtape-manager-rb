require_relative './helpers.rb'

RSpec.configure { |c| c.include Helpers }

RSpec.describe 'Mixtape' do
  # TODO: This doesn't work the way these tests are expecting.
  let(:mock_mixtape) { new_mock_mixtape }

  context 'when managing users' do
    let(:good_user_id) { '1' }
    let(:bad_user_id) { '999' }

    it 'should be able to find an existing user' do
      expect(mock_mixtape.find_user(good_user_id)).not_to be_nil
    end

    it 'should return nil when trying to find a nonexistent user' do
      expect(mock_mixtape.find_user(bad_user_id)).to be_nil
    end

    it 'should be able to determine whether a user exists' do
      user_exists = lambda { |id| mock_mixtape.user_exists?(id) }
      expect(user_exists.(good_user_id)).to be true
      expect(user_exists.(bad_user_id)).to be false
    end
  end

  context 'when managing playlists' do
    let(:good_playlist_id) { '1' }
    let(:good_playlist) do
      {
        id: good_playlist_id,
        user_id: '1',
        song_ids: ['1', '2', '3']
      }
    end
    let(:bad_playlist_id) { '999' }

    it 'should be able to find an existing playlist' do
      found = mock_mixtape.find_playlist(good_playlist_id)
      # expect(found).to eq(good_playlist)
      expect(found).not_to be_nil
    end

    it 'should return nil when trying to find a nonexistent playlist' do
      expect(mock_mixtape.find_playlist(bad_playlist_id)).to be_nil
    end

    it 'should be able to determine whether a playlist exists' do
      playlist_exists = lambda { |id| mock_mixtape.playlist_exists?(id) }
      expect(playlist_exists.(good_playlist_id)).to be true
      expect(playlist_exists.(bad_playlist_id)).to be false
    end
  end

  context 'when managing songs' do
    let(:good_song_id) { '1' }
    let(:bad_song_id) { '999' }

    it 'should be able to find an existing song' do
      found = mock_mixtape.find_song(good_song_id)
      expect(found).not_to be_nil
    end

    it 'should return nil when trying to find a nonexistent song' do
      expect(mock_mixtape.find_song(bad_song_id)).to be_nil
    end

    it 'should be able to determine whether a song exists' do
      song_exists = lambda { |id| mock_mixtape.song_exists?(id) }
      expect(song_exists.(good_song_id)).to be true
      expect(song_exists.(bad_song_id)).to be false
    end
  end

  it 'should be able to generate a next ID for a collection' do
    playlists = mock_mixtape.playlists
    playlist_ids = playlists.map { |pl| pl[:id] }
    max_id = playlist_ids.map(&:to_i).max
    expect(MixtapeManager::Mixtape.next_id(playlists)).to eq((max_id + 1).to_s)
  end

  it 'should be able to create a new playlist record' do
    user_id = '1'
    new_id = '3'
    song_ids = ['1', '2']
    new_playlist = mock_mixtape.make_playlist(user_id, song_ids)
    expected_playlist = {
      id: new_id,
      user_id: user_id,
      song_ids: song_ids
    }

    expect(new_playlist).to eq(expected_playlist)
  end
end
