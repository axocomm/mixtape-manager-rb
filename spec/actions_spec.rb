require_relative '../changes.rb'
require_relative './helpers.rb'

RSpec.configure { |c| c.include Helpers }

describe 'Mixtape actions' do
  before(:each) { @mock_mixtape = MixtapeManager::Mixtape.new(TEST_MIXTAPE) }

  describe 'when trying to add a new playlist' do
    it 'should be able to add a playlist with existing songs' do
      action = MixtapeManager::AddPlaylist.new(
        user_id: '1',
        song_ids: ['1', '2']
      )
      action.apply!(@mock_mixtape)

      new_id = '2'
      new_playlist = @mock_mixtape.find_playlist(new_id)
      expect(new_playlist).not_to be_nil
    end

    it 'should raise if a given song does not exist' do
      action = MixtapeManager::AddPlaylist.new(
        user_id: '1',
        song_ids: ['999']
      )

      expect { action.apply!(@mock_mixtape) }
        .to raise_error(MixtapeManager::Errors::NoSuchSongs)
    end

    it 'should raise if the user does not exist' do
      action = MixtapeManager::AddPlaylist.new(
        user_id: '999',
        song_ids: ['1']
      )

      expect { action.apply!(@mock_mixtape) }
        .to raise_error(MixtapeManager::Errors::NoSuchUser)
    end

    it 'should raise if no songs are provided' do
      action = MixtapeManager::AddPlaylist.new(user_id: '1', song_ids: [])
      expect { action.apply!(@mock_mixtape) }
        .to raise_error(MixtapeManager::Errors::MissingSongs)
    end
  end

  describe 'when trying to add a new song' do
    let(:playlist_id) { '1' }

    it 'should add an existing song to an existing playlist' do
      new_song_id = '4'
      action = MixtapeManager::AddSong.new(
        playlist_id: playlist_id,
        song_id: new_song_id
      )
      action.apply!(@mock_mixtape)

      playlist = @mock_mixtape.find_playlist(playlist_id)
      expect(playlist[:song_ids]).to include(new_song_id)
    end

    it 'should raise if the given playlist does not exist' do
      bad_playlist_id = '999'
      action = MixtapeManager::AddSong.new(
        playlist_id: bad_playlist_id,
        song_id: '1'
      )

      expect { action.apply!(@mock_mixtape) }
        .to raise_error(MixtapeManager::Errors::NoSuchPlaylist)
    end

    it 'should raise if the given song does not exist' do
      bad_song_id = '999'
      action = MixtapeManager::AddSong.new(
        playlist_id: playlist_id,
        song_id: bad_song_id
      )

      expect { action.apply!(@mock_mixtape) }
        .to raise_error(MixtapeManager::Errors::NoSuchSong)
    end

    it 'should not add a duplicate of a song if it already exists' do
      existing_song_id = '1'
      action = MixtapeManager::AddSong.new(
        playlist_id: playlist_id,
        song_id: existing_song_id
      )
      action.apply!(@mock_mixtape)

      playlist = @mock_mixtape.find_playlist(playlist_id)
      matches = playlist[:song_ids].select { |id| id == existing_song_id }
      expect(matches.count).to eq(1)
    end
  end

  describe 'when trying to remove a playlist' do
    it 'should be able to remove an existing playlist' do
      existing_playlist_id = '1'
      action = MixtapeManager::RemovePlaylist.new(
        playlist_id: existing_playlist_id
      )
      action.apply!(@mock_mixtape)

      removed_playlist = @mock_mixtape.find_playlist(existing_playlist_id)
      expect(removed_playlist).to be_nil
    end

    it 'should raise if the given playlist does not exist' do
      bad_playlist_id = '999'
      action = MixtapeManager::RemovePlaylist.new(
        playlist_id: bad_playlist_id
      )

      expect { action.apply!(@mock_mixtape) }
        .to raise_error(MixtapeManager::Errors::NoSuchPlaylist)
    end
  end
end
